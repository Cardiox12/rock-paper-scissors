const sign = ["rock", "paper", "scissor"];
const images = document.getElementById("signs").children;
const asset_signs = {
    "rock": images[0],
    "paper": images[1],
    "scissor": images[2]
}
let updated = false;
let computer_score = 0;
let player_score = 0;
let computer_choice;
let player_choice;
let round = 3;
const msg_box = document.getElementById("status-box-msg");
const score_box = document.getElementById("score-number");
const restart_btn = document.getElementById("restart-button");


function random(min, max) {
    if (min > max) {
        throw "Cannot generate random number with min > man";
    }
    return Math.floor((Math.random() * max) + min);
}

function playRound() {
    let index = random(1, 3);
    return sign[index - 1];
}

function printScore(score) {
    score_box.textContent = score;
}

function printStatus(msg, duration){
    msg_box.textContent = msg;
    setTimeout(() => {
        msg_box.textContent = "";
    }, duration);
    show_restart(true, duration + 100);
}

function playerIsWining(player, computer) {
    if (player == "rock" && computer == "scissor") {
        return true;
    } else if (player == "paper" && computer == "rock") {
        return true;
    } else if (player == "scissor" && computer == "paper") {
        return true;
    } else {
        false;
    }
}


function show_restart(show, duration){
    if (show){
        setTimeout(() => {
            restart_btn.classList.remove("hide");
        }, duration);
    } else {
        restart_btn.classList.add("hide");
    }
        
}

function onHoverAnimation(sign, duration){
    asset_signs[sign].classList.add("on-computer-hover");
    setTimeout(function () {
            asset_signs[sign].classList.remove("on-computer-hover");
    }, duration);
}

function initGame(round, player_choice) {

    if (round > 0 && player_score <= 15) {
        
        computer_choice = playRound()
        while (player_choice == computer_choice)
            computer_choice = playRound();
        
        onHoverAnimation(computer_choice, 1000);
        
        if (playerIsWining(player_choice, computer_choice)){
            player_score += 5;
            printScore(player_score, 2000);
        }
        else
            computer_score += 5;
        console.log("Je suis dans la boucle principale")

    } else if (round == 0){
        if (player_score > computer_score) {
            printStatus("Bravo vous avez gagné!", 2500);
            console.log("Bravo vous avez gagné!");
        } else {
            printStatus("Dommage vous avez perdu!", 2500);
            console.log("Dommage vous avez perdu!");
        }
        return;
    }
}

for (let image of images) {
    image.addEventListener("click", function () {
        initGame(round--, this.id);
    });
}

restart_btn.addEventListener("click", function() {
    player_score = 0;
    round = 3;
    console.log(round);
    printScore(player_score);
    show_restart(false, 0);
});